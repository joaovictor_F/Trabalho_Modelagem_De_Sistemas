package modelo;

import java.util.ArrayList;
import java.util.List;

public class Cliente {
	private String nome;
	private int cpf;
	private int telefone;
	private String dataDeCadastro;
	private Endereco tipo;
	
private List<Compra> compras = new ArrayList<Compra>();
	
	public void adicionarCompra(Compra compra){
		compras.add(compra);
	}
	

	public Cliente(String nome, int cpf, int telefone, String dataDeCadastro, Endereco tipo) {
		this.nome = nome;
		this.cpf= cpf;
		this.telefone = telefone;
		this.dataDeCadastro = dataDeCadastro;
		this.tipo = tipo;
	}
	
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}

	
	public int getCpf() {
		return cpf;
	}
	public void setCpf(int cpf) {
		this.cpf = cpf;
	}
	public int getTelefone() {
		return telefone;
	}
	public void setTelefone(int telefone) {
		this.telefone = telefone;
	}
	
	
	public String getDataDeCadastro() {
		return dataDeCadastro;
	}

	public void setDataDeCadastro(String dataDeCadastro) {
		this.dataDeCadastro = dataDeCadastro;
	}

	public Endereco getTipo() {
		return tipo;
	}

	public void setTipo(Endereco tipo) {
		this.tipo = tipo;
	}
	
	public void removerCompraPorNumero(int numero){
		for (Compra compra:compras){
			if (compra.getNumero() == numero ){
				compras.remove(numero);
			}
		}
	}	
	
	public Compra pegarCompraPorNumero(int numero){
		for (Compra compra: compras){
			if (compra.getNumero() == numero){
				return compra;
			}
		}
		return null; 
	}

	public List<Compra> getCompras(){
		return compras;
	}

}
