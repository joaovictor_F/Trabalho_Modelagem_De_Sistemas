package sistema;



import controlador.SistemaDeVendas;
import modelo.Endereco;

public class Principal {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		SistemaDeVendas controlador = SistemaDeVendas.getInstance();
		controlador.cadastrarProduto(1,"PS4", 1.200F,100,"01/05/2018");
		controlador.cadastrarProduto(2,"Controle Ps4",200.0F,150, "01/05/2018" );
		controlador.cadastrarProduto(3,"Fifa 18",150.0F,150, "01/05/2018" );
		controlador.cadastrarProduto(4,"Pes 18",100.0F,500,"01/05/2018" );
		
		controlador.cadastrarCliente( "Joao",250-55-2,98578-3052,Endereco.Residencial,"10/05/2018" );
		controlador.cadastrarCliente("Pedro",035-10-5, 3037-2964,Endereco.Comercial,"15/05/2018" );
		
		System.out.println("\n\n");		
		for (String str: controlador.listarProdutosCadastrados()){
			System.out.println(str);
		}
		
		for (String str: controlador.listarClientesCadastrados()){
			System.out.println(str);
		}
	
		
		controlador.selecionarClientePorNome("Joao");
		controlador.abrirCompra(1);
		controlador.venderProduto(1, "PS4", 1);
		controlador.venderProduto(1, "Fifa 18", 1);
		controlador.venderProduto(1, "Controle Ps4", 2);
	    System.out.println("Total da compra 1: "+controlador.totalizarVenda(1));
	    controlador.selecionarClientePorNome("Pedro");
		controlador.abrirCompra(2);
		controlador.venderProduto(2, "PS4", 3);
		controlador.venderProduto(2, "Pes 18 ", 1);
		controlador.venderProduto(2, "Controle Ps4", 2);
	    System.out.println("Total da compra 2: "+controlador.totalizarVenda(2));
		
	}

}
